package org.duttydev.timesheetoperator.commands;

import org.duttydev.timesheetoperator.misc.LocalDateConverter;
import org.duttydev.timesheetoperator.misc.StepResult;
import org.duttydev.timesheetoperator.services.TimeOperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CommandService {

    private static final String SUBMIT_CMD_NAME = "submit";
    private static final String HOURS_CMD_NAME = "hours";
    private static final List<String> SUPPORTED_COMMANDS = Arrays.asList(SUBMIT_CMD_NAME, HOURS_CMD_NAME );

    private static final String MON_HOURS_OPT = "--mon";
    private static final String TUE_HOURS_OPT = "--tue";
    private static final String WED_HOURS_OPT = "--wed";
    private static final String THU_HOURS_OPT = "--thu";
    private static final String FRI_HOURS_OPT = "--fri";
    private static final String SAT_HOURS_OPT = "--sat";
    private static final String SUN_HOURS_OPT = "--sun";

    private final TimeOperatorService timeOperatorService;

    @Autowired
    public CommandService(TimeOperatorService aTimeOperatorService) {
        timeOperatorService = aTimeOperatorService;
    }

    /**
     * Triggers the execution of the commands
     *
     * @param args cmd line args
     */
    public void execute(String[] args) throws Exception {

        List<String> argsList = Arrays.stream(args).collect(Collectors.toList());

        System.out.println("Validating arguments...");
        switch(argsList.get(0)) {
            case SUBMIT_CMD_NAME:
                submit(argsList).forEach(System.out::println);
                break;
            case HOURS_CMD_NAME:
                System.out.println(hours(argsList));
                break;
        }

    }

    /**
     * Validates the arguments for the submit command and passes the
     * validated values to operator service to submit timesheet
     *
     * @param args cmd line args
     * @return
     */
    private List<StepResult> submit(List<String> args) throws Exception {

        LocalDate finalDate = new LocalDateConverter().convert(args.get(1));
        Map<String,Integer> hours = new HashMap<>();
        hours.put(MON_HOURS_OPT,8);
        hours.put(TUE_HOURS_OPT,8);
        hours.put(WED_HOURS_OPT,8);
        hours.put(THU_HOURS_OPT,8);
        hours.put(FRI_HOURS_OPT,8);
        hours.put(SAT_HOURS_OPT,0);
        hours.put(SUN_HOURS_OPT,0);

        for (Map.Entry<String, Integer> hourEntry: hours.entrySet()) {
            if (args.contains(hourEntry.getKey())) {
                int markerIndex = args.indexOf(hourEntry.getKey());
                hours.put(hourEntry.getKey(), Integer.parseInt(args.get(markerIndex+1)));
            }
        }

        return timeOperatorService.submitTimesheet(finalDate,hours.get(MON_HOURS_OPT),
                hours.get(TUE_HOURS_OPT),hours.get(WED_HOURS_OPT),hours.get(THU_HOURS_OPT),
                hours.get(FRI_HOURS_OPT),hours.get(SAT_HOURS_OPT),hours.get(SUN_HOURS_OPT));
    }

    public StepResult hours(List<String> args) {
        return null;
    }

    public List<String> getSupportedCommands() {
        return SUPPORTED_COMMANDS;
    }

}
