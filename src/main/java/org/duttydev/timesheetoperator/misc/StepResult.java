package org.duttydev.timesheetoperator.misc;

import org.duttydev.timesheetoperator.actions.Action;

public class StepResult<T> {

    private final StepStatus status;
    private final Action step;
    private T value;

    public StepResult(StepStatus status, Action step) {
        this.status = status;
        this.step = step;
    }

    public StepResult(StepStatus status, Action step, T value) {
        this(status, step);
        this.value = value;
    }

    public StepStatus getStatus() {
        return status;
    }

    public Action getStep() {
        return step;
    }

    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        return status + " " + step + (value != null ? " => "+ value : "");
    }
}
