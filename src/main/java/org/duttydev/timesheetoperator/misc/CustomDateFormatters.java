package org.duttydev.timesheetoperator.misc;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;
import java.time.temporal.ChronoField;

public class CustomDateFormatters {

    public static final DateTimeFormatter TIMESHEET_LOCAL_DATE;
    static {
        TIMESHEET_LOCAL_DATE = new DateTimeFormatterBuilder()
                .appendValue(ChronoField.MONTH_OF_YEAR, 2)
                .appendLiteral('/')
                .appendValue(ChronoField.DAY_OF_MONTH, 2)
                .appendLiteral('/')
                .appendValue(ChronoField.YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
                .toFormatter();
    }

    public static final DateTimeFormatter EMAIL_TIMESHEET_SUBJECT_LOCAL_DATE;
    static {
        EMAIL_TIMESHEET_SUBJECT_LOCAL_DATE = new DateTimeFormatterBuilder()
                .appendValue(ChronoField.MONTH_OF_YEAR, 2)
                .appendLiteral('_')
                .appendValue(ChronoField.DAY_OF_MONTH, 2)
                .appendLiteral('_')
                .appendValue(ChronoField.YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
                .toFormatter();
    }
}
