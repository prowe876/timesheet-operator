package org.duttydev.timesheetoperator.misc;

import org.duttydev.timesheetoperator.misc.CustomDateFormatters;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

public class LocalDateConverter implements Converter<String,LocalDate> {

    @Override
    public LocalDate convert(String s) {
        return LocalDate.parse(s, CustomDateFormatters.TIMESHEET_LOCAL_DATE);
    }
}
