package org.duttydev.timesheetoperator.misc;

public enum StepStatus {
    SUCCESS("\u2714"), FAIL("\u2715"), DNS("\u002D");

    private final String symbol;

    StepStatus(String aSymbol) {
        symbol = aSymbol;
    }

    @Override
    public String toString() {
        return symbol;
    }
}
