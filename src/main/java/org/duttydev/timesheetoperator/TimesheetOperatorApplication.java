package org.duttydev.timesheetoperator;

import org.duttydev.timesheetoperator.commands.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimesheetOperatorApplication implements CommandLineRunner {

	@Autowired
	private CommandService commandService;

	public static void main(String[] args) {
		SpringApplication.run(TimesheetOperatorApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		if( !commandService.getSupportedCommands().contains(args[0]) ) {
			throw new IllegalArgumentException(args[0] + "command not supported");
		}

		commandService.execute(args);
	}

}
