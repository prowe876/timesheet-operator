package org.duttydev.timesheetoperator.services;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellAddress;
import org.duttydev.timesheetoperator.misc.CustomDateFormatters;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.FileAttribute;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;

/**
 * Created by pjrowe on 1/24/18.
 */
@Service
public class TimesheetService implements InitializingBean, DisposableBean {

    private final Environment environment;

    private String timesheetTemplatePath;
    private Pair<Integer,Integer> finalDateCell;
    private Pair<Integer,Integer> monHoursCell;
    private Pair<Integer,Integer> tueHoursCell;
    private Pair<Integer,Integer> wedHoursCell;
    private Pair<Integer,Integer> thuHoursCell;
    private Pair<Integer,Integer> friHoursCell;
    private Pair<Integer,Integer> satHoursCell;
    private Pair<Integer,Integer> sunHoursCell;

    @Autowired
    public TimesheetService(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        timesheetTemplatePath = environment.getRequiredProperty("TIMESHEET_TEMPLATE_LOCATION");

        finalDateCell = extractCoordinates(environment.getRequiredProperty("FINAL_DATE_CELL").split(","));
        monHoursCell = extractCoordinates(environment.getRequiredProperty("MON_HOURS_CELL").split(","));
        tueHoursCell = extractCoordinates(environment.getRequiredProperty("TUE_HOURS_CELL").split(","));
        wedHoursCell = extractCoordinates(environment.getRequiredProperty("WED_HOURS_CELL").split(","));
        thuHoursCell = extractCoordinates(environment.getRequiredProperty("THU_HOURS_CELL").split(","));
        friHoursCell = extractCoordinates(environment.getRequiredProperty("FRI_HOURS_CELL").split(","));
        satHoursCell = extractCoordinates(environment.getRequiredProperty("SAT_HOURS_CELL").split(","));
        sunHoursCell = extractCoordinates(environment.getRequiredProperty("SUN_HOURS_CELL").split(","));
    }

    @Override
    public void destroy() throws Exception {

    }

    Path fillOutTimesheet(LocalDate finalDate, Integer monHours, Integer tueHours, Integer wedHours,
                          Integer thuHours, Integer friHours, Integer satHours, Integer sunHours) throws Exception {

        final String tempDir = System.getProperty("java.io.tmpdir");
        final String timesheetName = "Timesheet_"+finalDate.format(CustomDateFormatters.EMAIL_TIMESHEET_SUBJECT_LOCAL_DATE);

        // Make copy of template
        Path tempTimesheet = Files.createFile(Paths.get(tempDir,timesheetName+".xls"));
        tempTimesheet = Files.copy(Paths.get(timesheetTemplatePath), tempTimesheet,
                StandardCopyOption.REPLACE_EXISTING);

        try ( InputStream stream = Files.newInputStream(tempTimesheet, StandardOpenOption.READ);
              OutputStream outputStream = Files.newOutputStream(tempTimesheet,StandardOpenOption.WRITE);
                Workbook timesheet = new HSSFWorkbook(stream) ) {

            Sheet firstSheet = timesheet.getSheetAt(0);

            // set final date
            setCell(finalDateCell, firstSheet, finalDate);

            // hours
            setCell(monHoursCell, firstSheet, monHours);
            setCell(tueHoursCell, firstSheet, tueHours);
            setCell(wedHoursCell, firstSheet, wedHours);
            setCell(thuHoursCell, firstSheet, thuHours);
            setCell(friHoursCell, firstSheet, friHours);
            setCell(satHoursCell, firstSheet, satHours);
            setCell(sunHoursCell, firstSheet, sunHours);

            HSSFFormulaEvaluator.evaluateAllFormulaCells(timesheet);

            timesheet.write(outputStream);
        }

        return Paths.get(tempTimesheet.toUri());
    }

    private Pair<Integer, Integer> extractCoordinates(String[] coord) {
        return new ImmutablePair<>(Integer.parseInt(coord[0]), Integer.parseInt(coord[1]));
    }

    private <T> void setCell(Pair<Integer, Integer> coordinates, Sheet excelSheet, T value) {

        Row row = excelSheet.getRow(coordinates.getLeft());
        Cell cell = row.getCell(coordinates.getRight());

        if(value.getClass().equals(LocalDate.class)) {
            cell.setCellValue(Date.from(((LocalDate)value).atStartOfDay(ZoneId.systemDefault()).toInstant()));
        } else {
            cell.setCellValue(((Integer)value).doubleValue());
        }
    }
}
