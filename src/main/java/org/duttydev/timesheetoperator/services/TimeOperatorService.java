package org.duttydev.timesheetoperator.services;

import org.duttydev.timesheetoperator.actions.*;
import org.duttydev.timesheetoperator.misc.CustomDateFormatters;
import org.duttydev.timesheetoperator.misc.StepResult;
import org.duttydev.timesheetoperator.misc.StepStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TimeOperatorService implements InitializingBean {

    private Logger LOGGER = LoggerFactory.getLogger(TimeOperatorService.class);

    private final Environment environment;
    private final EmailService emailService;
    private final StorageService storageService;
    private final TimesheetService timesheetService;

    private String userFullName;
    private List<String> recipients;
    private List<String> ccs;

    @Autowired
    public TimeOperatorService(Environment environment, EmailService emailService, StorageService storageService,
                               TimesheetService timesheetService) {
        this.environment = environment;
        this.emailService = emailService;
        this.storageService = storageService;
        this.timesheetService = timesheetService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        userFullName = environment.getRequiredProperty("USER_FULL_NAME");
        recipients = Arrays.stream(environment.getRequiredProperty("MAIL_RECIPIENTS").split(","))
                .collect(Collectors.toList());
        ccs = Arrays.stream(environment.getRequiredProperty("MAIL_CCS").split(","))
                .collect(Collectors.toList());
    }

    public List<StepResult> submitTimesheet(LocalDate finalDate, Integer monHours, Integer tueHours, Integer wedHours,
                                            Integer thuHours, Integer friHours, Integer satHours, Integer sunHours) throws Exception {


        Map<Action, StepResult> results = new EnumMap<>(Action.class);
        results.put(Action.FILL_OUT_TIMESHEET, new StepResult(StepStatus.DNS, Action.FILL_OUT_TIMESHEET));
        results.put(Action.STORE_TIMESHEET, new StepResult(StepStatus.DNS, Action.STORE_TIMESHEET));
        results.put(Action.EMAIL_TIMESHEET, new StepResult(StepStatus.DNS, Action.EMAIL_TIMESHEET));

        // Fill out timesheet
        System.out.println(Action.FILL_OUT_TIMESHEET + " in progress...");
        results.put(Action.FILL_OUT_TIMESHEET, fillOutTimesheet(finalDate, monHours, tueHours, wedHours, thuHours,
                friHours, satHours, sunHours));

        // Store timesheet
        StepResult fillOutTimesheetResult = results.get(Action.FILL_OUT_TIMESHEET);
        if (fillOutTimesheetResult.getStatus() == StepStatus.SUCCESS) {
            System.out.println(Action.STORE_TIMESHEET + " in progress...");
            results.put(Action.STORE_TIMESHEET, storeTimesheet((Path) fillOutTimesheetResult.getValue()));
        }

        // Email timesheet
        StepResult storeTimesheetResult = results.get(Action.STORE_TIMESHEET);
        if (storeTimesheetResult.getStatus() == StepStatus.SUCCESS) {

            String subject = emailSubject(finalDate);
            String body = emailBody();
            Path attachment = (Path) storeTimesheetResult.getValue();

            System.out.println(Action.EMAIL_TIMESHEET + " in progress...");
            results.put(Action.EMAIL_TIMESHEET, emailTimesheet(subject, body, attachment, recipients, ccs));
            Files.deleteIfExists(attachment);
        }

        return new ArrayList<>(results.values());
    }

    private StepResult<Path> fillOutTimesheet(LocalDate finalDate, Integer monHours, Integer tueHours, Integer wedHours,
                                              Integer thuHours, Integer friHours, Integer satHours, Integer sunHours) {

        StepResult<Path> result;

        try {
            Path timesheet = timesheetService.fillOutTimesheet(finalDate, monHours, tueHours, wedHours, thuHours,
                    friHours, satHours, sunHours);
            result = new StepResult<>(StepStatus.SUCCESS, Action.FILL_OUT_TIMESHEET, timesheet);
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to complete %s", Action.FILL_OUT_TIMESHEET), e);
            result = new StepResult<>(StepStatus.FAIL, Action.FILL_OUT_TIMESHEET);
        }

        return result;
    }

    private StepResult<Path> storeTimesheet(Path timesheet) {

        StepResult<Path> result;

        try {
            result = new StepResult<>(StepStatus.SUCCESS, Action.STORE_TIMESHEET, storageService.uploadTimesheet(timesheet));
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to complete %s", Action.STORE_TIMESHEET), e);
            result = new StepResult<>(StepStatus.FAIL, Action.STORE_TIMESHEET);
        }

        return result;
    }

    private String emailSubject(LocalDate finalDate) {
        return "Timesheet" + " " + finalDate.format(CustomDateFormatters.TIMESHEET_LOCAL_DATE) + " - " + userFullName;
    }

    private String emailBody() {
        final String newline = "<br>";
        return "Hi," +
                newline +
                newline +
                "The timesheet is attached as requested. It will be approved via email by manager." +
                newline +
                newline +
                "Thanks!" +
                newline +
                "Peter-John Rowe";
    }

    private StepResult<Void> emailTimesheet(String subject, String body, Path attachment, List<String> recipients,
                                            List<String> ccs) {

        StepStatus status;

        try {
            emailService.sendEmail(subject, body, attachment, recipients, ccs);
            status = StepStatus.SUCCESS;
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to complete %s", Action.EMAIL_TIMESHEET), e);
            status = StepStatus.FAIL;
        }

        return new StepResult<>(status, Action.EMAIL_TIMESHEET);
    }

}
