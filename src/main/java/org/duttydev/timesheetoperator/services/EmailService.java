package org.duttydev.timesheetoperator.services;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.EmailAddress;
import microsoft.exchange.webservices.data.property.complex.MessageBody;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

@Service
public class EmailService implements InitializingBean, DisposableBean {

    private final Environment environment;

    private ExchangeService exchangeService;

    private String workEmailUserName;

    @Autowired
    public EmailService(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        workEmailUserName = environment.getRequiredProperty("WORK_EMAIL_USER");
        String workEmailPassword = environment.getRequiredProperty("WORK_EMAIL_PWD");

        exchangeService = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
        exchangeService.setCredentials(new WebCredentials(workEmailUserName, workEmailPassword));
        exchangeService.autodiscoverUrl(workEmailUserName);
    }

    @Override
    public void destroy() throws Exception {
        exchangeService.close();
    }

    void sendEmail(String subject, String body, Path attachment, List<String> recipients,
                   List<String> ccs) throws Exception {

        EmailMessage message = new EmailMessage(exchangeService);

        message.setSubject(subject);
        message.setBody(MessageBody.getMessageBodyFromText(body));
        message.setFrom(EmailAddress.getEmailAddressFromString(workEmailUserName));

        for(String recipient: recipients) {
            message.getToRecipients().add(new EmailAddress(recipient));
        }

        for(String cc: ccs) {
            message.getCcRecipients().add(new EmailAddress(cc));
        }

        message.getAttachments().addFileAttachment(attachment.getFileName().toString(),
                Files.newInputStream(attachment, StandardOpenOption.READ));
        message.send();
    }

}
