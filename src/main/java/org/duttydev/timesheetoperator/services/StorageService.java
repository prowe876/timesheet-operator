package org.duttydev.timesheetoperator.services;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.core.auth.AwsCredentials;
import software.amazon.awssdk.core.auth.AwsCredentialsProvider;
import software.amazon.awssdk.core.auth.StaticCredentialsProvider;
import software.amazon.awssdk.core.regions.Region;
import software.amazon.awssdk.core.sync.ResponseInputStream;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by pjrowe on 1/23/18.
 */
@Service
public class StorageService implements InitializingBean, DisposableBean {

    private final Environment environment;
    private String timesheetS3Location;
    private S3Client s3Client;
    private ResponseInputStream<GetObjectResponse> responseInputStream;


    @Autowired
    public StorageService(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        final String accessKeyId = environment.getRequiredProperty("AWS_ACCESS_KEY_ID");
        final String accessKeySecret = environment.getRequiredProperty("AWS_SECRET_ACCESS_KEY");
        timesheetS3Location = environment.getRequiredProperty("TIMESHEETS_S3_LOCATION");

        AwsCredentialsProvider credentialsProvider = StaticCredentialsProvider
                .create(AwsCredentials.create(accessKeyId, accessKeySecret));

        s3Client = S3Client.builder()
                .credentialsProvider(credentialsProvider)
                .region(Region.US_EAST_1)
                .build();
    }

    @Override
    public void destroy() throws Exception {
        s3Client.close();
    }

    Path uploadTimesheet(Path timesheet) {
        s3Client.putObject(PutObjectRequest.builder().bucket(timesheetS3Location)
                        .key(timesheet.getFileName().toString()).build(),
                timesheet);

        return timesheet;
    }

//    void getTimesheets(LocalDate startDate, LocalDate endDate) {
//
//        ListObjectsV2Request request = ListObjectsV2Request.builder()
//                .bucket(timesheetS3Location)
//                .prefix("Timesheet_")
//                .build();
//
//        ListObjectsV2Response response = s3Client.listObjectsV2(request);
//
//        Predicate<? super S3Object> filterPredicate = (timesheet) -> {
//          boolean result = false;
//
//          String timesheetName = timesheet.key();
//
//          LocalDate timesheetEndDate = LocalDate.parse(timesheetName.split("Timesheet_")[0],
//                  CustomDateFormatters.EMAIL_TIMESHEET_SUBJECT_LOCAL_DATE);
//
//          startDate.
//
//          return result;
//        };
//
//        response.contents().stream().filter(a -> a.key().)
//    }
}
