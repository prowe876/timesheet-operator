package org.duttydev.timesheetoperator.actions;

/**
 * Action that encapsulates the name of an action that is performed
 *
 * @author pjrowe
 */
public enum Action {
    FILL_OUT_TIMESHEET("Fill Out Timesheet"), STORE_TIMESHEET("Store Timesheet"), EMAIL_TIMESHEET("Email Timesheet");

    private String value;

    Action(String aValue) {
        value = aValue;
    }

    @Override
    public String toString() {
        return value;
    }
}
