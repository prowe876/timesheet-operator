# Timesheet Operator

## About

The main purpose of this Timesheet Operator is to automate the submission
of my current timesheets for my job. It does this in the following 3 steps

1. Takes an excel (.xls) timesheet template and updates it with the 
timesheet date and the hours work per day
2. Stores a copy in S3
3. Sends an email with the timesheet as an attachment to recipients and 
cc'd individuals.

## How to submit a timesheet?

The build application will be run as an executable:

``./timesheet-operator``

The submit command has the following arguments:

- date of timesheet: mm/dd/yyyy
- hours for monday: --mon <hours> [ optional, default = 8 ]
- hours for tuesday: --tue <hours> [ optional, default = 8 ]
- hours for wednesday: --wed <hours> [ optional, default = 8 ]
- hours for thursday: --thu <hours> [ optional, default = 8 ]
- hours for friday: --fri <hours> [ optional, default = 8 ]
- hours for saturday: --sat <hours> [ optional, default = 0 ]
- hours for sunday: --sun <hours> [ optional, default = 0 ]

Examples can be seen below:

Submit a timesheet for a 40 hour week
    
            ./timesheet-operator submit 12/21/2017

Submit a timesheet where I worked half day on friday

            ./timesheet-operator submit 12/21/2017 --fri 4

Submit a timesheet where I worked 5 hours on monday and 6 hours on thursday

            ./timesheet-operator submit 12/21/2017 --mon 5 --thu 6

P.S. order of the hours don't matter

